const firstNum = +prompt("Введіть перше число: ");
const nextNum = +prompt("Введіть друге число: ");
const operation = prompt("Введіть операцію, яку потрібно виконати: ");

function executionOperation(){
  if (operation === "+"){
    return firstNum + nextNum;
  } else if (operation === "-"){
    return firstNum - nextNum;
  } else if(operation === "*"){
    return firstNum * nextNum;
  } else {
    return firstNum / nextNum;
  }

}
let result = executionOperation(firstNum, nextNum);
console.log(result);